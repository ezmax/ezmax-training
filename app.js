require("babel-polyfill");
const ex1 = require('./js/5.1.1.js');
const ex2 = require('./js/5.1.2.js');
const ex3 = require('./js/5.2.js');
const ex4 = require('./js/5.3.js');
const Book = require('./js/5.4.1.js');
const PaperbackBook = require('./js/5.4.2.js');
const ex6 = require('./js/5.5.js');
const ex7 = require('./js/5.6.js');
const ex8 = require('./js/5.7.js');
const ex9 = require('./js/5.8.js');
const ex10 = require('./js/5.10.js');
//const ex11 = require('./js/5.14.3b.js');
//comment

/*ex1.f1();
ex1.f2();*/

//ex2.f1();

/*ex3.f1();
ex3.f2();
ex3.f3();*/

/*ex4.f1();
ex4.f2();*/

//valider si le nombre de param�tres est valid� par Typescript
/*var bk = new Book("Ocean Mer", "Alessandro Baricco", 512, 158720);
console.log(bk);
console.log(`Words per page: ${bk.wordsPerPage}`);
console.log(`Cover type: ${bk.coverType}`);

var pb = new PaperbackBook("L'�cume de jours", "Boris Vian", 722, 223820);
console.log(pb);
console.log(`Words per page: ${pb.wordsPerPage}`);
console.log(`Cover type: ${pb.coverType}`);
console.log(`Static method: ${Book.wordsPerPage(500, 50000)}`);*/

/*ex6.f1();
ex6.f2();*/

/*ex7.f1();
ex7.f2();*/

/*ex8.f1();
ex8.f2();*/

/*ex9.f1();
ex9.f2();*/

/*ex10.f1();
ex10.f2();*/

let book = new Book("The Stand", "Stephen King", 823, 472376);
/*book.queryDatabase('READ').then(
	(message) => console.log(message)
);*/

// First READ the existing record.
// Once READ is successful then perform an UPDATE.
// Output the resolve messages for each call.
/*book.queryDatabase('READ').then((message) => {
	console.log(message);
	return book.queryDatabase('UPDATE');
}).then(
	(message) => console.log(message)
);*/

// Try to destroy book database record.
/*book.queryDatabase('DESTROY').then(
	// Output the resolve message, if applicable.
	(message) => console.log(message),
	// Catch any errors or failures and output that message instead.
	(errorMessage) => console.log(errorMessage)
);*/

// Ensure that CREATE, READ, and UPDATE are all successful.
/*Promise.all([
	book.queryDatabase('CREATE'),
	book.queryDatabase('READ'),
	book.queryDatabase('UPDATE')
	//, book.queryDatabase('DESTROY')
]).then(
	(message) => console.log(`CREATE, READ, and UPDATE succeeded for [${book.title} by ${book.author}].`),
	(errorMessage) => console.log(errorMessage)
);*/

// Check which operation is fulfilled (or rejected) first.
/*Promise.race([
	book.queryDatabase('CREATE'),
	book.queryDatabase('READ'),
	book.queryDatabase('UPDATE'),
	book.queryDatabase('DESTROY')
]).then(
	(message) => console.log(message),
	(errorMessage) => console.log(errorMessage)
);
console.log('apres');*/

//ex11.f1();