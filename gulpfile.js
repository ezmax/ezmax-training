var gulp = require('gulp');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var babel = require('gulp-babel');
var gutil = require('gulp-util');


gulp.task('default', function() {
  // votre code ici
});

// Lint Task
gulp.task('lint', function() {
  return gulp.src('./erreurs.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('scripts', function() {
  return gulp.src('js/5.14.3.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('dist'))
    .pipe(rename('all.min.js'))
    .pipe(babel())
    .pipe(uglify())
    .on('error', function (err) { 
    	gutil.log(gutil.colors.red('[Error]'), err.toString()); 
    })
    .pipe(gulp.dest('dist/js'));
});

//npm install babel-core --save-dev
//npm install gulp-babel
gulp.task('build', function () {
  return gulp.src('./js/5.14.3.js')
    .pipe(babel())
    .pipe(rename('5.14.3b.js'))
    .pipe(gulp.dest('./js'))
})
