var Book = require('./5.4.1.js');

class PaperbackBook extends Book {
	constructor(...args) {
		super(...args);
		this.coverType = "paperback";
	}
}
		
module.exports = PaperbackBook;