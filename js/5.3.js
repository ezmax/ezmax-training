var starPoem5 = 'Resistance is futile,\n\t'
    + 'You will be assimilated.'
var starPoem6 = `Resistance is futile,
    You will be assimilated. `;
    
var f1 = function() {
	console.log(starPoem5);
}

var f2 = function() {
	console.log(starPoem6);
}

exports.f1 = f1;
exports.f2 = f2;