function getBookArray() {
	return [
		'The Hobbit',
		'J. R. R. Tolkien', 
		320
	];
}

function getBookObject() {
	return {
		title: 'The Hobbit',
		author: 'J. R. R. Tolkien',
		page_count: 320
	};
}

var f1 = function() {
		//ES5
		var tempBookArray = getBookArray(), title = tempBookArray[0], author = tempBookArray[1], page_count = tempBookArray[2];
		console.log(`${title} by ${author} [${page_count} pages]`);
		var tempBookObject = getBookObject(), title = tempBookObject.title, author = tempBookObject.author, page_count = tempBookObject.page_count;
		console.log(`${title} by ${author} [${page_count} pages]`);
}

var f2 = function() {
		//ES6
		var [ title, author, page_count ] = getBookArray();
		console.log(`${title} by ${author} [${page_count} pages]`);
		var { title, author, page_count } = getBookObject();
		console.log(`${title} by ${author} [${page_count} pages]`);
}

exports.f1 = f1;
exports.f2 = f2;