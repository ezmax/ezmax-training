class Book {
	constructor(title, author, pageCount, wordCount) {
		this.title = title;
		this.author = author; 
		this.pageCount = pageCount;
		this.wordCount = wordCount;
	}

	get wordsPerPage() {
		return this.wordCount / this.pageCount;
	}

	static wordsPerPage(pageCount, wordCount) {
		return wordCount / pageCount;
	}
	
	// CRUD
		  queryDatabase(type) {
		    type = type || 'CREATE';
		    // Return a new Promise to be asynchronously handled elsewhere.
		    return new Promise((resolve, reject) => {
		      // Using setTimeout() to simulate an async call
		      if (type == 'CREATE') {
		        setTimeout(() => {
		          resolve(`Creating ${this.title} by ${this.author}.`);
		        }, 1200);
		      } else if (type == 'READ') {
		        setTimeout(() => {
		          resolve(`Reading ${this.title} by ${this.author}.`);
		        }, 1000);
		      } else if (type == 'UPDATE') {
		        setTimeout(() => {
		          resolve(`Updating ${this.title} by ${this.author}.`);
		        }, 800);
		      } else if (type == 'DESTROY') {
		        setTimeout(() => {
		          reject(new Error(`Destruction of [${this.title} failed.`));
		        }, 1000);
		      }
		    });
		  }
}
		
module.exports = Book;