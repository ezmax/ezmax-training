var firstName = 'Jean-Michel', lastName = 'David';
var getFullName = function (lastName = 'LAST', firstName = 'FIRST') {
	return `${firstName} ${lastName}`; //litt�raux de mod�le
}

var f1 = function() {
	console.log(`Full name is ${firstName} ${lastName}`);
}

var f2 = function() {
	console.log(`Full name is ${getFullName('David', 'Jean-Michel')}`);
}

var f3 = function() {
	console.log(`One plus two equals ${1+2}`);
}

exports.f1 = f1;
exports.f2 = f2;
exports.f3 = f3;