function multiplier(facteur, ...lesArguments) {
	return lesArguments.map(function (element) {
		return facteur * element;
	});
}

var f1 = function() {
	var arr = multiplier(2, 1, 2, 3);
	console.log(arr);
}

exports.f1 = f1;