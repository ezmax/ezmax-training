function *myGenerator() {
	console.log("Pre-yield!");
	yield 50;
	console.log("Post-yield!");
}
var myGenerator = myGenerator();
		
var f1 = function() {
		console.log(myGenerator.next().value);
		console.log("Doing other, out of scope stuff...");
		console.log(myGenerator.next().value);
}

function *doubler() {
	var value = 1;
	while (true) {
		value *= 2;
		yield value;
	}
}
var doubler = doubler();

var f2 = function() {
		console.log(doubler.next().value);
		console.log(doubler.next().value);
		console.log(doubler.next().value);
		console.log(doubler.next().value);
}

exports.f1 = f1;
exports.f2 = f2;