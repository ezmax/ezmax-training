var list = [3, 5, 7];
list.foo = 'bar';

var f1 = function() {
		for (var key in list) {
		  console.log(`${key} : ${list[key]}`); // 0, 1, 2, foo
		  //console.log(list[key]);
		}
}

var f2 = function() {
		//nouveau en ES6
		for (var value of list) {
		  console.log(value); // 3, 5, 7
		}
}

exports.f1 = f1;
exports.f2 = f2;