// Create book objects.
let book1 = { title: "Chaos calme", author: "Sandro Veronesi", pageCount: 281 };
let book2 = { title: "Les racines du ciel", author: "Romain Gary", pageCount: 584 };

var f1 = function() {
	// Create library map object.
	let library = new Map();
	// Set published dates using book objects as keys.
	library.set(book1, new Date(2000, 6, 11));
	library.set(book2, new Date(1958, 0, 1));

	// Get published dates.
	console.log(`'${book1.title}' by ${book1.author} published on ${library.get(book1).toDateString()}.`);
	console.log(`'${book2.title}' by ${book2.author} published on ${library.get(book2).toDateString()}.`);
	console.log(library);
}

var f2 = function() {
	// Create library set object.
		let library = new Set();
		// Add book1 to set.
		library.add(book1);
		// Determine if books exists in set.
		console.log(`Is '${book1.title}' by ${book1.author} in the library? ${library.has(book1) ? 'YES' : 'NO'}`);
		console.log(`Is '${book2.title}' by ${book2.author} in the library? ${library.has(book2) ? 'YES' : 'NO'}`);

		// Add book2 to set.
		library.add(book2);
		// Delete book1 from set.
		library.delete(book1);

		// Determine if books exists in set.
		console.log(`Is '${book1.title}' by ${book1.author} in the library? ${library.has(book1) ? 'YES' : 'NO'}`);
		console.log(`Is '${book2.title}' by ${book2.author} in the library? ${library.has(book2) ? 'YES' : 'NO'}`);
		console.log(library);
}

exports.f1 = f1;
exports.f2 = f2;