var f1 = function() {
		var name = "Alice";
		{
		  var name = "Bob";
		  console.log(`Name is ${name}.`);  //Name is Bob
		}
		console.log(`Name is ${name}.`); //Name is Bob
}

var f2 = function() {
		var name = "Alice";
		{
		  let name = "Bob";
		  console.log(`Name is ${name}.`); //Name is Bob
		}
		console.log(`Name is ${name}.`); //Name is Alice
}

var printError = function(error) {
	console.log(`${error.name}: ${error.message}`);
}

var f3 = function() {
		try {
		  const e = 2.71828;    
		  e = 2.7; //erreur : �TypeError: Assignment to constant variable.�
		} catch (e) {
		  printError(e, true);
		}
}

exports.f1 = f1;
exports.f2 = f2;
exports.f3 = f3;